import sublime
import sublime_plugin
import os
from xml.etree import ElementTree as ET
import json
import time
import re

class MysqlAbstarctCommand(sublime_plugin.WindowCommand):
    
    input = None
    input_callback = None

    select_options = None
    select_callback = None
    
    def show_select(self, options, callback = None):
        self.select_callback = callback
        self.select_options = options
        self.select = self.window.show_quick_panel(self.select_options, self._on_select_done)

    def _on_select_done(self, index):
        try:
            value = self.select_options[index]
            # print value
            self.on_select_done(index, value)
        except Exception, e:
            print e

    def on_select_done(self, index, value):
        if self.select_callback != None:
            self.select_callback(index, value)

    def show_input(self, label = "", default = "", callback = None):
        
        self.input_callback = callback
        self.input = self.window.show_input_panel(
            label,
            default,
            self.on_input_done,
            self.on_input_change,
            self.on_input_cancel
        )

    def on_input_done(self, value):
        if self.select_callback != None:
            self.select_callback(index, value)
        pass

    def on_input_change(self, value):
        pass

    def on_input_cancel(self):
        pass


    def settings_set(self, key, value):
        settings = sublime.load_settings('Mysql.sublime-settings')
        settings.set(key, value)
        sublime.save_settings('Mysql.sublime-settings')

        # print key + ' = ' + value

    def settings_get(self, value, default = None):
        settings = sublime.load_settings('Mysql.sublime-settings')
        return settings.get(value, default)

class MysqlManageCommand(MysqlAbstarctCommand):

    def run(self):
        self.show_select([
            "Connection: Select",
            "Connection: Add",
            "Connection: Remove",
            "Database: Select",
            "Output: Set format"
        ], self.on_select_done_main)
        
    def on_select_done_main(self, index, value):
        
        if index == 0:
            configs = self.settings_get("connections", {}).keys() 
            self.show_select(configs, self.on_select_done_select)

        elif index == 1:
            self.show_input("Add Connect (name: user pass [host])")

        elif index == 2:
            configs = self.settings_get("connections", {}).keys() 
            self.show_select(configs, self.on_select_done_remove)

        elif index == 3:
            self.mysql = MySQL()
            dbs = self.mysql.do_query("SHOW DATABASES", "list")
            self.show_select(dbs, self.on_select_done_db)

        elif index == 4:
            self.show_select(["table", "pretty-table", "json", "xml", "html"], self.on_select_done_format)

    def on_select_done_format(self, index, value):
        self.settings_set('output_format', value)


    def on_select_done_db(self, index, value):
        self.settings_set("connection_database", value[0]);

    def on_select_done_remove(self, index, value):

        configs = self.settings_get("connections") 
        del configs[value]
        self.settings_set("connections", configs) 

    def on_select_done_select(self, index, value):
        self.settings_set("connection", value)

    def on_input_done(self, text):
        try:
            m = re.match('^([A-Za-z]*): ([^\s]*) ([^\s]*)$', text)
            if m:
                configs = self.settings_get("connections") 
                configs[m.group(1)] = {
                    "host": "localhost",
                    "user": m.group(2),
                    "pass": m.group(3)
                }
                self.settings_set("connections", configs)
                return

        except Exception, e:
            pass

        self.show_input("Add Connect (name: user pass [host])")





class MysqlShowTablesCommand(MysqlAbstarctCommand):
    
    mysql = None
    def run(self):
        self.mysql = MySQL()
        self.show_select(self.mysql.query_tables())

    def on_select_done(self, index, value):

        tbl = value[0]
        self.show_input("Query", "SELECT * FROM " + tbl + " LIMIT 100")

    def on_input_done(self, query):
        self.mysql.query(query)


class MysqlQuerySelectionCommand(sublime_plugin.TextCommand):  
    def run(self, edit):  
        braces = False  
        sels = self.view.sel()

        queries = ""
        for sel in sels:  
            queries += self.view.substr(sel) + ";\n"

        mysql = MySQL()
        mysql.query(queries)

class MysqlQueryFileCommand(sublime_plugin.TextCommand):   
    def run(self, edit):  
        
        filename = self.view.file_name()
        if filename:
            pass

            mysql = MySQL()
            mysql.query_file(filename)

class MysqlQueryPromptCommand(MysqlAbstarctCommand):
    def run(self):
        self.show_input("Query", "")

    def on_input_done(self, text):
        mysql = MySQL()
        mysql.query(text)



class OutputPanel:
    
    def __init__(self, text, panel_name, type = ""):
        self.window = sublime.active_window()

        if not hasattr(self, 'output_view'):
            self.output_view = self.window.get_output_panel(panel_name)
        v = self.output_view

        v.set_read_only(0)

        edit = v.begin_edit()
        v.insert(edit, v.size(), text + '\n')
        v.end_edit(edit)

        s = v.settings()
        s.set('rulers', [])
        s.set('word_wrap', False)
        s.set('wrap_width', 0)
        s.set('line_numbers', False)

        
        if type == 'table' or type == 'pretty-table':
            v.set_syntax_file("Packages/SublimeMySQL/sublime-mysql.tmLanguage")

        if type == 'json':
            v.set_syntax_file("Packages/JavaScript/JSON.tmLanguage")

        if type == 'xml':
            v.set_syntax_file("Packages/XML/XML.tmLanguage")

        if type == 'html':
            v.set_syntax_file("Packages/HTML/HTML.tmLanguage")
        
        v.set_read_only(1)

        v.show(0)
                
        self.window.run_command("show_panel", {"panel": "output." + panel_name})
        v.run_command('revert');


class MySQL:
    
    config = None
    def __init__(self, config = None):
        self.settings = sublime.load_settings('Mysql.sublime-settings')

        config_key = self.settings.get("connection", "default")
        configs = self.settings.get("connections", {})
        
        if config_key in configs:
            self.config = configs[config_key]
        else:
            self.config = {"":"localhost", "user":"", "pass":""}

    def query(self, query, format = None, output = None):

        tmpfile_path = "/tmp/qery.sql"
        
        try:
            f = open(tmpfile_path, "w")
            try:
                f.write(query) # Write a string to a file
            finally:
                f.close()
        except IOError:
            pass


        return self.query_file(tmpfile_path, format, output)


    def query_file(self, file_path, format = None, output = None):
        
        if not format:
            format = self.settings.get('output_format', 'table')

        if not output:
            output = self.settings.get('output_open', 'output_panel')

        
        s = self.do_query("source '" + file_path + "'", format)
        if not s:
            s = "[ ERROR ]"
        
        out = ""

        if self.settings.get('output_header', True):
            out += time.strftime("%d.%m.%Y %H:%M:%S")
            out += "\n"
            out += "-"*80 + "\n"
            out += "\n"
            out += self.read_file(file_path) + "\n"
            out += "\n"
            out += "-"*80 + "\n"
            out += "\n"
        
        out += s


        output = self.settings.get('output_open', 'output_panel')

        if output == 'output_panel':
            OutputPanel(out, "mysql", format)
        if output == 'tab':
            OutputPanel(out, "mysql", format)
        else:
            try:
                fname = "/tmp/dump.txt"
                if format == 'html':
                    fname += '.html'
                f = open(fname, "w")
                try:
                    f.write(out) # Write a string to a file
                finally:
                    f.close()
                cmd = output + ' ' + fname

                # print cmd
                p = os.popen(cmd)
                p.close()

            except IOError:
                pass

    
    def do_query(self, query, format):

        try:
            bin = "/Applications/MAMP/Library/bin/mysql"
            
            args = []
            args.append("-u" + self.config["user"])
            args.append("-p" + self.config["pass"])
            args.append(self.settings.get("connection_database", ""))


            args.append("--batch")
            if format == 'table':
                args.append("--table")

            elif format == 'xml' or format == 'json' or format == 'dict' or format == 'list' or format == 'pretty-table':
                args.append("--xml")

            elif format == 'html':
                args.append("--html")

            args.append("-e '" + query + "'")

            cmd = bin + " " + " ".join(args)

            p = os.popen(cmd)
            s = p.read()
            p.close()

            if format == 'json' or format == 'dict' or format == 'list' or format == 'pretty-table':

                if format == 'list' or format == 'pretty-table' or (format == 'json' and self.settings.get("output_json_numeric", False)):
                    numeric = True
                else:
                    numeric = False

                col_names = []
                first = True

                resultset = ET.fromstring(s)
                json_data = []
                for row in resultset:

                    if numeric:
                        row_data = []
                        for field in row:
                            row_data.append(field.text)
                            if first:
                                col_names.append(field.attrib['name'])
                        json_data.append(row_data)

                    else:
                        row_data = {}
                        for field in row:
                            row_data[field.attrib['name']] = field.text
                            if first:
                                col_names.append(field.attrib['name'])
                        json_data.append(row_data)

                    first = False

                if format == 'json':
                    s = json.dumps(json_data, indent=4)
                else:
                    s = json_data



                # print json_data
                # 
                
                if format == 'pretty-table':
                    
                    widths = [0]*1000;
                    
                    if format == 'pretty-table':

                        json_data.insert(0, col_names)
                        for row in json_data:
                            
                            i = 0
                            for col in row:
                                try:
                                    col_rows = col.decode('utf-8').split("\n")
                                except Exception, e:
                                    col_rows = []
                                for col_row in col_rows:
                                    widths[i] = max(widths[i], len(col_row))
                                row[i] = col_rows

                                i = i+1


                        t = ''
                        t += self.gen_divider(widths, '=')

                        row_i  = 0;
                        for row in json_data:
                            
                            has_more = True
                            col_i = 0
                            
                            while has_more:
                                has_more = False

                                t +=  '| '


                                i = 0
                                for col_rows in row:
                                    w = widths[i]
                                    try:
                                        val = col_rows[col_i]
                                    except Exception:
                                        val = ""
                                    
                                    t += val.ljust(w)[:w] + " | "

                                    if len(col_rows) > col_i+1:
                                        has_more = True

                                    if col_i > 10:
                                        break

                                    i = i+1

                                t += "\n"

                                col_i = col_i+1

                            if row_i == 0:
                                t += self.gen_divider(widths, "=")
                            else:
                                t += self.gen_divider(widths)
                            row_i = row_i +1;

                        s = t


            return s

        except Exception, e:
            print query
            print e
            pass

        return None

    def gen_divider(self, widths, char = '-'):

        d = '+ '
        i = 0;
        while widths[i] > 0:
            d += char*widths[i]
            d += ' + '
            i = i+1
        d += "\n"
        return d

    def read_file(self, fname):

        if os.path.exists(fname):
            try:
                f = open(fname)
                content = f.read()
                f.close()
                return "" + content

            except Exception, e:
                pass
        
        return ""

    def query_tables(self):
        return self.do_query("SHOW TABLES", "list")
